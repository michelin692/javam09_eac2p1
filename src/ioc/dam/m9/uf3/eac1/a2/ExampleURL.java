/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf3.eac1.a2;

/**
 *
 * @author Usuari
 */
import java.io.*;
import java.net.URL;

public class ExampleURL {
    public static void main(String[] args) throws IOException {
        URL web = new URL("https://developer.apple.com/news/rss/news.rss");
        System.out.println("Autoritat:\t"+web.getAuthority());
        System.out.println("Port per defecte:\t"+web.getDefaultPort());
        System.out.println("Recurs:\t"+web.getFile());
        System.out.println("Host:\t"+web.getHost());
        System.out.println("Camí:\t"+web.getPath());
        System.out.println("Protocol:\t"+web.getProtocol());

        web.openConnection();

        BufferedReader in = new BufferedReader( new InputStreamReader(web.openStream()));

        File f = new File("blog.xml");
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(f));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            if(inputLine.contains("<title>")) {
                System.out.println(inputLine);
                bw.write(inputLine + "\n");
            }
        }
        bw.close();
        in.close();

    }
}
