package ioc.dam.m9.uf3.eac1.b1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 
 */
public class ServidorAritmetic {

    protected ServeiAritmetic servei;
    protected Socket socket;
    DataInputStream in;
    DataOutputStream out;
    
    public void setServei(ServeiAritmetic servei) {
        this.servei = servei;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void executa() {
            
            double resultat;
        try {
            //Llegeix el missatge del client i l'analitza
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            resultat = this.analitza(in.readUTF());
            
            //Escriu el resultat cap al client
             System.out.println("El resultat de l'operacio es: " + resultat);  
            //Tanca el flux
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorAritmetic.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    //El protocol per a la operació és operador:valor1:valor2
    private double analitza(String operacio) throws IllegalArgumentException {
        //System.out.println("antes: " + operacio);
        String[] arrayValores;
        arrayValores = operacio.split(":");
        double valor = 0;
        //System.out.println("despues: " + arrayValores[1]);
        switch( arrayValores[0] ){
            case "+":
               valor = servei.suma(Double.valueOf(arrayValores[1]), Double.valueOf(arrayValores[2]));
            break;
            case "-":
               valor = servei.resta(Double.valueOf(arrayValores[1]), Double.valueOf(arrayValores[2]));
            break;
            case "*":
               valor = servei.mult(Double.valueOf(arrayValores[1]), Double.valueOf(arrayValores[2]));
            break;
            case "/":
               valor = servei.div(Double.valueOf(arrayValores[1]), Double.valueOf(arrayValores[2]));
            break;
        }
        
        return valor;
    }

    
    public static void main(String[] args) throws Exception {
        
        
        // Crea el socket servidor i espera la connexió del client
        ServidorAritmetic servidorAritmetic;
        Socket s;
        ServerSocket serverSocket = null;
        serverSocket = new ServerSocket(9999);
        System.out.println("El servidor matemàtic está executant...");
        System.out.println("Esperant clients...");
        
        while(true){
            
            s = serverSocket.accept();
            System.out.println("Cliente conectado");
            servidorAritmetic = new ServidorAritmetic();
            servidorAritmetic.setServei(new ServeiAritmeticImpl());
            servidorAritmetic.setSocket(s);
            servidorAritmetic.executa();
            
            s.close();
            
        }
        
        
        
        
        
    }
}
