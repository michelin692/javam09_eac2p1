package ioc.dam.m9.uf3.eac1.b1;

/**
 *
 * @author 
 */
public interface ServeiAritmetic {

    public double suma(double valor1, double valor2);

    public double resta(double valor1, double valor2);

    public double div(double valor1, double valor2);

    public double mult(double valor1, double valor2);

}
