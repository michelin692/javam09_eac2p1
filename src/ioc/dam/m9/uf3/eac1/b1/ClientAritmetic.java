/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf3.eac1.b1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 
 */
public class ClientAritmetic {

    public static void main(String[] args) {
        
        try {
            //Envia la operació
            DataInputStream in;
            DataOutputStream out;
            
            Socket s = new Socket("localhost",9999);
            
            in = new DataInputStream(s.getInputStream());
            out = new DataOutputStream(s.getOutputStream());
            // Obté el resultat del servidor i l'imprimeix
            out.writeUTF("+:121:11");
            
            String mensajeFinal = in.readUTF();
            System.out.println(mensajeFinal);
            //desconectamos
            s.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ClientAritmetic.class.getName()).log(Level.SEVERE, null, ex);
        }
                    
    }
            
}
