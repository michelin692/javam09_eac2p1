package ioc.dam.m9.uf3.eac1.b2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuari
 */
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class JavaLookup
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //
        String host = "www.ioc.cat";
        System.out.println(lookup(host));
    } //final del main
    
    private static String lookup(String host){
        InetAddress node;
        String resultat = "";
        try {
            node = InetAddress.getByName(host);
            if(isHostname(host)){
                //System.out.println("Entro host");
                resultat = node.getHostAddress();
            }else{
                //System.out.println("No entro host");    
                resultat = node.getHostName();
            }
            //obtenir els bytes de les adreçes
        } catch (UnknownHostException ex) {
            Logger.getLogger(JavaLookup.class.getName()).log(Level.SEVERE, null, ex);
        }

        //ens passa de host a ip
        //ens passa de ip a host.
        return resultat;
    }
     // 

    private static boolean isHostname(String host) {
        
        // si veiem un caracter que no és ni un dígit ni un punt
        // llavors host és probablement és un hostname
        char ArrayString[];
        boolean isHost = false;
        ArrayString = host.toCharArray();
        
        for (int x=0;x<ArrayString.length;x++){
             if(!Character.isDigit(ArrayString[x])){
                 if( ArrayString[x] != ".".toCharArray()[0] ){
                    isHost = true;
                 };
             };
        }
        
        return isHost;
        
        
    }
    
}